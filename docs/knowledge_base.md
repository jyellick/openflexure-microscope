# OpenFlexure Knowledge Base

The OpenFlexure Microscope instructions aim to explain not only how to assemble your microscope but also why is was designed that way. Throughout the instructions links to information pages use this information signal: [i](#)

Available pages in the knowledge base.

{{listpages, tag:knowledge}}